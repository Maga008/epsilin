from django.db import models
from django.utils import timezone
from django.shortcuts import reverse
from django.contrib.auth.models import User


class Category(models.Model):
    title = models.CharField("Sarlovha", max_length=255)
    slug = models.SlugField("Silka", unique=True)
    image = models.ImageField("Rasm", blank=True, null=True)
    

    class Meta:
        verbose_name = 'Kategoriya'
        verbose_name_plural = 'Kategoriyalar'

    def get_absolut_url(self):
        return reverse('category_detail_url', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

class Post(models.Model):
    title = models.CharField("Sarlovha", max_length=255)
    slug = models.SlugField("Silka", unique=True)
    image = models.ImageField("Rasm", blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Kategoriya")
    content = models.TextField("Text")
    date = models.DateTimeField("Sana",default=timezone.now)
    publish = models.BooleanField("Taqdim etish", default=False)

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Postlar'

    def get_absolut_url(self):
        return reverse('post_detail_url', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title
    

class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Avtor")
    post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name="Yangilik")
    text = models.TextField("Kommentariya")
    data = models.DateTimeField("Sana", default=timezone.now)

    class Meta:
        verbose_name = "Komentariya"
        verbose_name_plural = "Komentariyalar"

    def __str__(self):
        return self.user.username
